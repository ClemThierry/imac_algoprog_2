#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    int i = heapSize;
    this->set(i, value);
    while (i>0 && this->get(i)>this->get((i-1)/2)) {
        this->swap(i, ((i-1)/2));
        i=(i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    int indexMax = nodeIndex,largest,left = -1;
    if (this->leftChild(indexMax)<=heapSize-1) {
        left = this->get(this->leftChild(indexMax));
    }
    int right = -1;
    if (this->rightChild(indexMax)<=heapSize-1) {
        right = this->get(this->rightChild(indexMax));
    }

    if (this->get(indexMax)<left) {
        if (right<left) {
            largest = this->leftChild(indexMax);
        }
        else {
            largest = this->rightChild(indexMax);
        }

    } else if ((this->get(indexMax))<right) {
        largest = this->rightChild(indexMax);
    } else {
         largest = indexMax;
    }

    if (largest != indexMax) {
        this->swap(indexMax, largest);
        heapify(heapSize, largest);
    }

}

void Heap::buildHeap(Array& numbers)
{
    int taille = numbers.size();
    for (int i=0; i<taille; i++) {
    this->insertHeapNode(i, numbers[i]);
    }

}

void Heap::heapSort()
{
   for(int i= this->size()/2 -1; i>= 0; i--){
       this->heapify(size(),i);
   }
    for (int i=this->size()-1; i>0; i--) {
        this->swap(0, i);
        this->heapify(i, 0);
    }
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
    w->show();

    return a.exec();
}
