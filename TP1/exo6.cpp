#include <iostream>

using namespace std;

// LISTES CHAINEES
struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    int nbNoeud;
};

void initialise(Liste* liste)
{
    liste->premier = NULL;
    liste->nbNoeud = 0;
}

bool est_vide(const Liste* liste)
{
    if(liste->nbNoeud == 0){
        return true;
    }else{
        return false;
    }
}

void ajoute(Liste* liste, int valeur)
{
    Noeud * nvNoeud = (Noeud*)malloc(sizeof (*nvNoeud));
    nvNoeud->donnee = valeur;
    nvNoeud->suivant = NULL;


    if(liste->premier == NULL){
        liste->premier = nvNoeud;
    }else{
        Noeud* tmp = liste->premier;
        while (tmp->suivant != NULL) {
            tmp=tmp->suivant;
        }
        tmp->suivant = nvNoeud;
    }
    liste->nbNoeud = liste->nbNoeud + 1;
}


void affiche(const Liste* liste)
{
    int i = 1;
    Noeud * tmp = liste->premier;

    while(tmp != NULL){
        cout << "Valeur n " << i << " : " << tmp->donnee<< endl;
        tmp = tmp->suivant;
        i++;
    }
    cout << endl;
}

int recupere(const Liste* liste, int n)
{
    if(n>=0 && n<=liste->nbNoeud){
        Noeud * tmp = liste->premier;

        for(int i=0; i<n; i++){
            tmp = tmp->suivant;
        }

        return tmp->donnee;
    }else{
        return -1;
    }
}

int cherche(const Liste* liste, int valeur)
{
    int i = 0, index = -1;
    bool trouve = false;

    Noeud * tmp = liste->premier;

    while(!trouve && tmp != NULL){
        if(tmp->donnee == valeur){
            index = i;
            trouve = true;
        }
        tmp = tmp->suivant;
        i++;
    }
    if(!trouve){
       return -1;
    }else{
      return index;
    }
}

void stocke(Liste* liste, int n, int valeur)
{
    if(n>=0 && n<=liste->nbNoeud){
        Noeud * tmp = liste->premier;

        for(int i=0; i<n; i++){
            tmp = tmp->suivant;
        }

        tmp->donnee = valeur;
    }else{
        cout << "Erreur(le rang n'existe pas)" << endl;
    }
}

// TABLEAUX DYNAMIQUES
struct DynaTableau{
    int* donnees;
    int nbDonnees;
    int capacite;
};

void ajoute(DynaTableau* tableau, int valeur)
{
    if(tableau->nbDonnees == tableau->capacite){
        int * tmp = new int[tableau->capacite + 1];

        for(int i=0; i<tableau->capacite; i++){
            tmp[i] = tableau->donnees[i];
        }
        tmp[tableau->capacite] = valeur;

        free(tableau->donnees);

        tableau->donnees = tmp;
        tableau->capacite = tableau->capacite + 1;
    }else{
        tableau->donnees[tableau->nbDonnees] = valeur;
    }
    tableau->nbDonnees = tableau->nbDonnees + 1;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;
    tableau->donnees = new int[capacite];
    tableau->nbDonnees = 0;
}

bool est_vide(const DynaTableau* tableau)
{
    if(tableau->nbDonnees == 0){
        return true;
    }else{
        return false;
    }
}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i<tableau->nbDonnees; i++){
        cout << "Valeur n " << i << " : " << tableau->donnees[i] << endl;
    }
    cout << endl;
}

int recupere(const DynaTableau* tableau, int n)
{
    if(n>=0 && n <tableau->nbDonnees){
        return tableau->donnees[n];
    }else{
        cout << "Erreur(le rang n'existe pas)" << endl;
        return -1;
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int i = 0, index = -1;
    bool trouve = false;

    while(!trouve && i < tableau->nbDonnees){
        if(tableau->donnees[i] == valeur){
            index = i;
            trouve = true;
        }
        i++;
    }
    if(!trouve){
       return -1;
    }else{
      return index;
    }
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if(n>=0 && n <tableau->nbDonnees){
         tableau->donnees[n] = valeur;
    }else{
        cout << "Erreur: le rang demandé n'existe pas dans le tableau" << endl;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    Noeud * tmp = liste->premier;
    int valeur = tmp->donnee;

    liste->premier = liste->premier->suivant;
    liste->nbNoeud = liste->nbNoeud - 1;
    free(tmp);

    return valeur;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud * nvNoeud = new Noeud;

    nvNoeud->donnee = valeur;
    nvNoeud->suivant = liste->premier;
    liste->premier = nvNoeud;
    liste->nbNoeud = liste->nbNoeud + 1;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    Noeud * tmp = liste->premier;
    int valeur = tmp->donnee;

    liste->premier = liste->premier->suivant;
    liste->nbNoeud = liste->nbNoeud - 1;
    free(tmp);

    return valeur;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans le tableau à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
