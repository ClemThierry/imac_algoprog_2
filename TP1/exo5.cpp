#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot
    if(n==0)
    {
        return 0;
    }

    float reel=z.x;
    z.x=z.x*z.x - z.y*z.y + point.x
    z.y=2*reel*z.y+point.y;

    if(z.length()>2)
    {
        return 1;
    }
    else{
        return isMandelbrot(z,n-1,point);
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



