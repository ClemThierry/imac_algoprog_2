#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if(origin.size()<=1){
        return;
    }else{
        // initialisation
        Array& first = w->newArray(origin.size()/2);
        Array& second = w->newArray(origin.size()-first.size());

        // split
        int j=0;
        for(int i=0; i<origin.size();i++){
            if(i<first.size())
            {
               first[i]=origin[i];
            }else{
                second[j]=origin[i];
                j++;
            }
        }

        // recursiv splitAndMerge of lowerArray and greaterArray
        splitAndMerge(first);
        splitAndMerge(second);

        // merge
        merge(first, second, origin);

    }
}

void merge(Array& first, Array& second, Array& result)
{
    int j = 0;
    int k = 0;

    for(int i=0; i<result.size();i++){
        if (j<first.size() && k<second.size()) {
            if (first[j]<=second[k]) {
                result[i]=first[j];
                j++;
            }
            else {
                result[i]=second[k];
                k++;
            }
        }
        else if (j>=first.size()) {
            result[i]=second[k];
            k++;
        }
        else if (k>=second.size()) {
            result[i]=first[j];
            j++;
        }

    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
