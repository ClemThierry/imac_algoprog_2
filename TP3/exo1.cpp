#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;

int binarySearch(Array& array, int toSearch)
{
    int i=0;
    int n = array.size();
    int mid;
    while (i<n)
    {
        mid=(i+n)/2;
        if(toSearch>array[mid])
        {
            i=mid+1;
        }else if (toSearch<array[mid])
        {
            n=mid;
        }else
        {
            return mid;
        }
    }
	return -1;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchWindow(binarySearch);
	w->show();

	return a.exec();
}
